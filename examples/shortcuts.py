# code.py (JoyPad)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates using the JoyPad
in advanced mode where you control the keys and joystick data.

This configuration is great for creating shortcuts for programs
such as OBS Studio, MS Flight Simulator, a Video Editor, etc.

This example is for VSCode.
The shortcuts are from: https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf

This example maps most of the JoyPad keys to editor functions.
It also keeps the joystick for scrolling.

Documentation: 
**Hardware:**
    * the JoyPad (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

try:
    from joypad import JoyPad
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/joypad.py'")
    print ("The latest JoyPad library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/joypad")
    print("------------------------------------------------------------------------")

# create the jp (JoyPad) object in advanced mode with the LEDs off (BLACK)
jp = JoyPad(color=JoyPad.BLACK, advanced=True)
jp.turbo = JoyPad.OFF
jp.scroll = JoyPad.ON

jp.title('VSCode')
#              "123456789012345678901" this is the maximum length of a text line
jp.textline(0, "UFLD FOLD FRMT SAV*")
jp.textline(1, "SPLT SWCH CLOS WRAP")
jp.textline(2, "LIST SRCH GIT  MKDN")
jp.textline(3, "nERR pERR          ")
jp.textline(4, "BLD  UPLD TMNL     ")

# IMPORTANT: the JoyPad uses a US keyboard layout. See documentation.
from adafruit_hid.keycode import Keycode # Provides access to predefine keycodes

# a list of Keycodes is available here: 
# https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode
# use None for the buttons you wish to ignore

# these are examples of a compound key sequence; alternately, these could be placed directly in the key_sequences list below
# keycodes with parenthesis represent keys which are all pressed together
# keycodes in brackets represent keys which are all pressed sequentially
# these can also be combined
# refer to README.md and API.md for more details and examples

unfold = (Keycode.CONTROL, Keycode.SHIFT, Keycode.RIGHT_BRACKET)
fold = (Keycode.CONTROL, Keycode.SHIFT, Keycode.LEFT_BRACKET)
format = (Keycode.SHIFT, Keycode.ALT, Keycode.F)
saveall = [(Keycode.CONTROL, Keycode.K), Keycode.S]
split_window = (Keycode.CONTROL, Keycode.BACKSLASH)
split_toggle = (Keycode.SHIFT, Keycode.ALT, Keycode.ZERO)
close_split = [(Keycode.CONTROL, Keycode.K), Keycode.W]
toggle_wrap = (Keycode.ALT, Keycode.Z)
file_tab = (Keycode.CONTROL, Keycode.SHIFT, Keycode.E)
search_tab = (Keycode.CONTROL, Keycode.SHIFT, Keycode.F)
git_tab = (Keycode.CONTROL, Keycode.SHIFT, Keycode.G)
show_markdown = [(Keycode.CONTROL, Keycode.K), Keycode.V]
next_error = Keycode.F8
prev_error = (Keycode.SHIFT, Keycode.F8)
run_build = (Keycode.CONTROL, Keycode.ALT, Keycode.B)
run_upload = (Keycode.CONTROL, Keycode.ALT, Keycode.U)
show_terminal = (Keycode.CONTROL, Keycode.GRAVE_ACCENT)

# each key sequence may be a keycode, series of keycodes, keycode combinations, astring, or None
key_sequences = (\
    unfold, fold, format, saveall, \
    split_window, split_toggle, close_split, toggle_wrap, \
    file_tab, search_tab, git_tab, show_markdown, \
    next_error, prev_error, None, None, \
    run_build, run_upload, show_terminal, \
)

while True:
    jp.update() # this is very important so everything has a chance to check for new information

    x, y, s = jp.movement  # get the calculated mouse movement; we only use scroll for this example
    if s:
        jp.mousemove(0,0,s)

    #if any keys are pressed, then take action for each one
    while jp.keys_changed is True:
        key = jp.key.number
        led = jp.key.raw
        action = jp.key.pressed

        # turn the LED on while the key is pressed
        jp.led(led, (JoyPad.BLUE if action else jp.color))

        # ignore the key 'released' action
        if not action:
            continue
        # send key commands over USB
        jp.send(key_sequences[key])
    # end while loop of new keypad events 

