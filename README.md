# JoyPad

<img align="right" style="padding: 0 0 0 12px;" src="files/joypad_512.jpg"/>

The JoyPad is a programmable keypad with a game controller styled joystick.

The JoyPad is a USB device running a CircuitPython program.

Plug the JoyPad into a computer and the sample program creates a USB mouse and USB keypad.

But that is just the beginning. The JoyPad is customizable and programmable so it can be used for almost anything you can imagine! Use the JoyPad to speed up your video editing with shortcuts and macros. Create dedicated keys for your favorite video games. Even use the JoyPad as a Midi input device.

The JoyPad behavior and features are totally customizable using CircuitPython - an easy to use programming language. And if you really don't want to learn to program, that's OK too. The JoyPad is preprogrammed as a keypad and making small changes is a breeze without learning a bunch of code.

If you are already familiar with the JoyPad and want to really customize it then take a look at the [**API**](API.md) for all the configuration and capabilities.

[[_TOC_]] 


## Hardware Under the Hood

The JoyPad incorporates the [Raspberry Pi PICO](https://www.raspberrypi.com/products/raspberry-pi-pico/) to control the joystick, keys, display, LEDs, and buzzer.

- The joystick is the same as those used by many game controllers.
- The keys for the keypad are MX-style mechanical 'clicky' keys.
- There is an addressable RGB LED under each key and one under the joystick.
- There display is a 128x64 pixel OLED screen.
- There is also a buzzer capable of playing simple tones.

The game controller style joystick has a limited range - it can be too fast or too slow, and it's rarely just right. To solve this, the JoyPad has mapping curves. There are two curves for mouse movement and two curves for scroll speed. The normal motion is set to be quick.

In the sample `code.py` the top four keys work with the joystick to emulate a mouse.
- The left two keys are the left and right mouse buttons.
- When the third key is held down, the joystick switches to be a scroll wheel.
- When the forth key is help down, the mouse movement toggles Turbo Mode to move slower for more accuracy. _(The Turbo Mode can be swapped so the mouse is slow by default and the press makes it quick.)_

The JoyPad is [available](https://www.tindie.com/stores/bradanlane/) fully assembled. There are options for blank key caps, laser engraved key caps, and acrylic faceplate with mounting hardware. _(as shown in picture)_

## Software

The JoyPad runs [CircuitPython](https://circuitpython.org/) - a programming language designed to be easy to learn and run on low-cost hardware devices.

The JoyPad comes with all the necessary software pre-installed. It's software may be upgraded at anytime. The necessary files include:
- [CircuitPython for the Raspberry Pi PICO](https://circuitpython.org/board/raspberry_pi_pico/)
- Libraries from the [Adafruit CircuitPython Bundle](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases)
- [The JoyPad library, fonts, and sample code.py](https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/joypad)


### Getting Started with CircuitPython

If you are new to programming CircuitPython or not familiar with programming at all, then 
a great place to start is [Welcome to CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) from Adafruit.

CircuitPython is easy to learn or just to make small changes.
You edit the `code.py` file with any text editor _such as **Notepad** (Windows) or **TextEdit** (Mac) or **Nano** (Linux)_ or you can use 
an editor like [**Mu**](https://learn.adafruit.com/welcome-to-circuitpython/installing-mu-editor) which has special features to make CircuitPython programming even easier.
You can read more about editing code for CircuitPython in [Adafruit's guide](https://learn.adafruit.com/welcome-to-circuitpython/creating-and-editing-code).

If you are new to CircuitPython, it's important to know that indentations are important. CircuitPython - _like Python_ - uses indentation to organize code
and code blocks. The recommended method of indenting is to always use spaces. Editors like **Mu** make this easy.

### Flow of the Code

Here is a typical CircuitPython JoyPad program ...

<img align="center" style="width: 90%" src="files/flow_diagram.png"/>

The **setup** is where we create our JoyPad object and establish the initial behavior. Some things to setup now are the initial display title and text, default color of the LEDs, and the joystick movement speed.

Now the program will **begin** a _loop_ which will run forever. Everything the JoyPAd will do, it will be inside this loop.

>>>
Remember that note above about indentation? The instructions within the loop are identified by their level of indentation. Similar, where there is an _if_ statement or another loop, the code statements for them are indented again. The examples below will give you a chance to work with the code and become familiar how CircuitPython relies on indentation to group code statements.
>>>

Inside the **begin** loop the program it needs to call _update()_ to give the JoyPad an opportunity to perform periodic tasks like getting new data from the joystick, checking for key presses/releases, and updating the screen or LEDs.

Now that the JoyPad has been updated, there is another _loop_ to let the program take action for all of the _keys_changed_ - eg any keys which have been pressed or released since the previous _update()_.

Inside this loop the code gets a changed key and works with it. In the sample `code.py` there is a table of keycodes and macros corresponding to the keys on the JoyPad. When a key press occurs, the _send()_ instruction sends the corresponding keycode or macro to the computer.

The end of the indentation level for _keys_changed_ loop causes the execution to go back to the start of the loop and look for more keys. Once all of the key presses and releases have been processed, this loop ends and the execution goes back to _begin_.

Here is what this looks like in CircuitPython ...
```python
# SETUP
from joypad import JoyPad
from adafruit_hid.keycode import Keycode

key_sequences = (\
    Keycode.SEVEN,  Keycode.EIGHT,  Keycode.NINE,   Keycode.ESCAPE, \
    Keycode.FOUR,   Keycode.FIVE,   Keycode.SIX,    None, \
    Keycode.ONE,    Keycode.TWO,    Keycode.THREE,  Keycode.ENTER, \
    None,           Keycode.ZERO,   Keycode.PERIOD, \
)

jp = JoyPad(color=JoyPad.WHITE)
jp.turbo = False

jp.title('JoyPad')

#BEGIN (loop)
while True:

	#UPDATE
    jp.update()
	
	# KEYS_CHANGED (loop)
    while jp.keys_changed:
    	# KEY (get data)
        key = jp.key.number
        led = jp.key.led
        action = jp.key.pressed

        if not action:
            continue
            
		# SEND (keycode or macro)
        jp.send(key_sequences[key])
    # END loop
# END loop
```

## Learn to Customize

The `code.py` from this project provides an easy starting point for any customization you may want.

The keypad has a total of 19 keys.
The default layout uses the top row for mouse related tasks.
There are 10 numbers, the PERIOD, ESCAPE, and the ENTER key.
That leaves 2 keys with nothing to do.

You don't need to keep the default layout. You could change the ESCAPE or PERIOD keys, or any other keys for your needs.

_It is also possible to control all 19 keys, including the top row (see the `advanced` property below)._

When you plug the JoyPad into the computer USB for the first time, three things appear. The JoyPad will appear as a mouse device. It will also appear as a keyboard device. There will also be a new USB storage device called `CIRCUITPY`.

The JoyPad runs [CircuitPython][circuitpython] which is an easy and approachable programming language. No special tools are needed.

Browse the `CIRCUITPY` device. You will see something that looks like this ...
```
Directory of CIRCUITPY:
├── font
│   ├── profont12.pcf
│   ├── profont17.pcf
├── lib
│   ├── adafruit_bitmap_font
│   │   ├── **
│   ├── adafruit_display_text
│   │   ├── **
│   ├── adafruit_hid
│   │   ├── **
│   ├── adafruit_displayio_ssd1306.mpy
│   ├── neopixel.mpy
│   ├── joypad.py
├── code.py
```
_There may be other files and folder. You can ignore them._

### Example: Changing a Key

The first customization you will want to make is to give one of the three unassigned keys a purpose. Let's add the plus singe (+) to one of the key right above the 'ENTER' key.


The `code.py` file is where you will make all of your changes. Start by copying the file to a safe place _(if we make a mistake, you can just copy this safe version back to `CIRCUITPY`.)_

Now, open `code.py` in your text editor.

Let's go ahead and add that plus sign (+) to the JoyPad.

In the `code.py` you should see some code that looks like this (it's at the bottom):

```python
key_sequences = (\
    Keycode.SEVEN,  Keycode.EIGHT,  Keycode.NINE,   Keycode.ESCAPE, \
    Keycode.FOUR,   Keycode.FIVE,   Keycode.SIX,    None, \
    Keycode.ONE,    Keycode.TWO,    Keycode.THREE,  Keycode.ENTER, \
    None,           Keycode.ZERO,   Keycode.PERIOD, \
)
```

These are the keycodes which correspond to the keys on the keypad (starting with the second row). You see there are three without any keycode. Instead these have `None`. Let's put the plus sign (+) in the spot right about `Keycode.ENTER`. Adafruit has a page of all possible [keycodes](https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode).

Modify this code so that it looks like this:

```python
key_sequences = (\
    Keycode.SEVEN,  Keycode.EIGHT,  Keycode.NINE,   Keycode.ESCAPE, \
    Keycode.FOUR,   Keycode.FIVE,   Keycode.SIX,    Keycode.KEYPAD_PLUS, \
    Keycode.ONE,    Keycode.TWO,    Keycode.THREE,  Keycode.ENTER, \
    None,           Keycode.ZERO,   Keycode.PERIOD, \
)
```

**Note:** CircuitPython is _whitespace-sensitive_. That means it pays attention to indentation. To be safe, always use spaces (not tabs) to indent lines. Fortunately, there is no indentation to `key_sequences`. But, you will see it in the last example.

Back to our example ...

Save the file! The JoyPad should restart automatically. Now, try that new key on the keypad.

### Example: Toggle Joystick Speed

The joystick emulates a mouse in the sample `code.py`. It's pretty quick with is great for moving around a big screen. Holding down the "SPEED" key (top row, right most key) switches from fast to a slow movement. What if you want the default to be slow and the key to make it fast? Let's make that change.

Open `code.py` in your text editor.

The default Turbo Mode behavior is controlled by a _property_ in the JoyPad library.

In the `code.py` you should see some code that looks like this (it's at the bottom):

```python
jp = JoyPad(color=JoyPad.WHITE)

jp.title('JoyPad')
```

The first line shown, is where our JoyPad object is created. It gives us access to all of the JoyPad capabilities. The second line is an example of one of those capabilities - we use a _method_ to set the title on the screen.

Let's use another capability of the JoyPad to change its mouse movement behavior.

Modify this code so that it looks like this:

```python
jp = JoyPad(color=JoyPad.WHITE)
jp.turbo = JoyPad.OFF

jp.title('JoyPad')
```

We have just set the JoyPad default Turbo Mode to off which means the mouse movement speed will be slower. Now the "SPEED" key will switch from slow to fast.

Save the file! The JoyPad should restart automatically. Now, try that new behavior of the joystick.

### Example: Change the JoyPad Color

All of the LEDs are white. We could change them to another color **and** we could change the color of the LED for a key when it is pressed. Let's look at those two changes.

Find the following line in `code.py`:

```python
jp = JoyPad(color=JoyPad.WHITE)
```

This is where the JoyPad object is first created. We can set the color for all the LEDs here.
You can make up your own color but for now, choose one of the pre-defined colors. _(the full set  of predefined colors are listed below)_
 
Modify this code so that it looks like this (but use the color of your choosing):

```python
jp = JoyPad(color=JoyPad.ORANGE)
```

Save the file! The JoyPad should restart automatically. Now, behold the glow of your new color!

### Example: Change LED Color on Key Press

OK, now let's change the color of an LED when a key is pressed.

At the very bottom of `code.py` is where all the action takes place. Find the following lines:

```python
while True:
    jp.update() # this is very important so everything has a chance to check for new information
    #if any keys are pressed, then take action for each one

    while jp.has_keys:
        key = jp.key.number
        led = jp.key.raw
        action = jp.key.pressed

        # ignore the key 'released' action
        if not action:
            continue

        # send key commands over USB
        jp.send(key_sequences[key])
    # end while loop of new keypad events 

# end while True (forever)
```

There are two `while` statements here. These form loops where the code will run and when it reaches th end, it will go back to the `while`. The first one is `while True:` which is always going to happen so this loops will go on forever. A few lines down (and indented) is `while jp.has_keys:` and this loop will keep going as long as it has key presses and key releases to work on.

Everything that is indented within `while jp.has_keys:` is part of its loop. The first few lines setup some variables.

We want to change the LED color when there is an **action** aka a key has been pressed. We also want to return the LED to it's original color when a key has been released.

Modify this code so that it looks like this (but use the color of your choosing):

```python
while True:
    jp.update() # this is very important so everything has a chance to check for new information
    #if any keys are pressed, then take action for each one

    while jp.has_keys:
        key = jp.key.number
        led = jp.key.raw
        action = jp.key.pressed

        # change the LED red while the key is pressed
        if action:
            jp.led(led, JoyPad.RED)
        else:
            jp.led(led, jp.color)

        # ignore the key 'released' action
        if not action:
            continue

        # send key commands over USB
        jp.send(key_sequences[key])
    # end while loop of new keypad events 

# end while True (forever)
```

If you are familiar with Python programming, here is another way to code the LED change:

```python
        # the LED red while the key is pressed
        jp.led(led, (JoyPad.RED if action else jp.color))
```

**Tip:** the lines beginning with the pound sign (#) are comments and an easy way to add notes to your code.

Almost forgot, this is where indentation with spaces is important. You can see the `if action:` has been indented to the same level as the lines above. The line with `jp.led(led, JoyPad.RED)` is indented more. If we had more lines of code which should run when `action is True`, they would be indented the same amount as `jp.led(led, JoyPad.RED)`. The `else:` is only indented as much as the `if action:`. This indicates the end of all the lines for our `if` section and the beginning of the code to run when `action is False`.

Save the file! The JoyPad should restart automatically. Now, press a key and see the LED change then release the key and see it change back.

There is a lot more you can do but now you have a little practice customizing the JoyPad.

### Example: Simple Music Keypad

There is a fun music keypad ([musicdemo.py](examples/musicdemo.py)) which demonstrates some of the ways to use the advanced mode of the JoyPad. To load it onto the JoyPad, copy the file to `CIRCUITPY` with the destination name `code.py`

## JoyPad API

All of the configuration and capabilities are documented in the [**API**](API.md).

**Request:** If you use the JoyPad and find something missing from the API, you are encouraged to create an [_issue_](https://gitlab.com/bradanlane_cp/joypad/-/issues) in the gitlab project. Please describe what you are attempting to do and what is needed from the API. You are also free to make your own copy of the `joypad.py` library.

## IMPORTANT UPDATES

There are no updates (yet) to mention.