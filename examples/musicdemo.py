# code.py (JoyPad)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates using the JoyPad
in advanced mode where you control the keys and joystick data.

Documentation: 
**Hardware:**
    * the JoyPad (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

try:
    from joypad import JoyPad
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/joypad.py'")
    print ("The latest JoyPad library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/joypad")
    print("------------------------------------------------------------------------")

# create the jp (JoyPad) object in advanced mode with the LEDs off (BLACK)
jp = JoyPad(color=JoyPad.BLACK, advanced=True)


jp.title('MusicPad')
#              "123456789012345678901" this is the maximum length of a text line
jp.textline(0, " C4   D4   E4   F4 ")
jp.textline(1, " G4   A4   B4   C5 ")
jp.textline(2, " D5   E5   F5   G5 ")
jp.textline(3, " A5   B5   C6   D6 ")
jp.textline(4, " E6   F6   G6      ")

# we want to compute the pitch_bend as +/- one note from the base_frequency
# in order to support both negative and positive pitch bending, the notes list needs extending both ends
# the notes table will support a max bend of 2 notes but we are only using 1 in the code below
notes = [233, 247, 262, 294, 330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175, 1319, 1397, 1568, 1661, 1760]

# a list of actives keys; initially all keys are inactive
keys = [0 for i in range(20)]
key_count = 1

def pitch_bend(key, bend):
    bend = -bend
    # bend is a raw joystick value initially a big number 0..32K
    key += 2
    frequency = notes[key]
    if bend < 0:
        delta = notes[key-1] - frequency
    else:
        delta = frequency - notes[key+1]
    delta = delta * (bend / 32000)
    frequency -= int(delta)
    #print("key #%d  bend %+5d below %4d freq %4d above %4d" % (key, bend, frequency, notes[key-1], notes[key+1]))
    return frequency

active_key = -1
while True:
    jp.update() # this is very important so everything has a chance to check for new information

    x, y = jp.position  # get the raw joystick positioning; we only use Y axis for this demo

    while jp.keys_changed:
        active_key = jp.key.number
        led = jp.key.raw
        action = jp.key.pressed

        # turn the LED red while the key is pressed
        jp.led(led, (JoyPad.BLUE if action else jp.color))

        if action:
            # on press we mark the key active and increment our counter
            keys[active_key] = key_count
            key_count += 1
            jp.toneon(pitch_bend(active_key, y))
        else:
            # on key release, we clear the key from the list
            keys[active_key] = 0
            key_count -= 1
            # and now look for the 'next most recent' key
            val = max(keys)
            if val > 0:
                active_key = keys.index(val)
                jp.toneon(pitch_bend(active_key, y))
            else:
                jp.toneoff()
                active_key  = -1
    # end while loop of new keypad events

    # handle no new key changes but we still have an active key and want to process pitch bend
    if y and active_key >= 0:
        # a fun pitch bending example
        jp.toneon(pitch_bend(active_key, y))

# end while True (forever)
