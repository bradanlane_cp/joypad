# code.py (JoyPad)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the JoyPad as a keypad and mouse.

The JoyPad hardware connects via USB and provides:
    - joystick movement and a push button
    - a 4x5 keypad using a numeric keypad layout
    - a 128x64 OLED display
    - a simple tone buzzer
    - a small mass storage device (used to store the code and related files)

The sample code used the movement to simulate a USB mouse
with the top row of keys providing L/R buttons, scroll mode, and accelerated movement mode,
and the remaining keys are a traditional numeric keypad.

Documentation: 
**Hardware:**
    * the JoyPad (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

try:
    from joypad import JoyPad   # REQUIRED: import the JoyPad library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/joypad.py'")
    print ("The latest JoyPad library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/joypad")
    print("------------------------------------------------------------------------")

# IMPORTANT: the JoyPad uses a US keyboard layout. See documentation.
from adafruit_hid.keycode import Keycode # Provides access to predefine keycodes

# a list of Keycodes is available here: 
# https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode
# use None for the buttons you wish to ignore

# this is an example of a compound key sequence; it will clear out a field in a form
clearL = [Keycode.HOME, (Keycode.SHIFT, Keycode.END), Keycode.DELETE]

# each key sequence may be a keycode, series of keycodes, keycode combinations, astring, or None
key_sequences = (\
    Keycode.SEVEN,  Keycode.EIGHT,  Keycode.NINE,   Keycode.ESCAPE, \
    Keycode.FOUR,   Keycode.FIVE,   Keycode.SIX,    None, \
    Keycode.ONE,    Keycode.TWO,    Keycode.THREE,  Keycode.ENTER, \
    None,           Keycode.ZERO,   Keycode.PERIOD, \

)

jp = JoyPad(color=JoyPad.WHITE)
jp.turbo = JoyPad.OFF

jp.title('JoyPad')
#              "123456789012345678901" this is the maximum length of a text line
jp.textline(0, "LEFT RGHT SCRL SPEED")
jp.textline(1, " 7    8    9     ESC")
jp.textline(2, " 4    5    6        ")
jp.textline(3, " 1    2    3     ENT")
jp.textline(4, "      0    .        ")

while True:
    jp.update() # this is very important so everything has a chance to check for new information
    #if any keys are pressed, then take action for each one
    while jp.keys_changed is True:
        key = jp.key.number
        led = jp.key.raw
        action = jp.key.pressed

        # ignore the key 'released' action
        if not action:
            continue
        # send key commands over USB
        jp.send(key_sequences[key])
    # end while loop of new keypad events 

# end while True (forever)
