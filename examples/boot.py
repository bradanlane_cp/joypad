# if you do not want the JoyPad to appear as a mass storage device, use this code
# it will default to disabling the mass storage device CIRCUITPY
# it looks for the joystick button pressed at startup to enable mass storage

import board
import storage
import digitalio

button = digitalio.DigitalInOut(board.GP22)
button.pull = digitalio.Pull.UP

if button.value:
    storage.disable_usb_drive()
