# JoyPad

<img align="right" style="padding: 0 0 0 12px;" src="files/joypad_512.jpg"/>

The JoyPad is a programmable keypad with a game controller styled joystick.

The JoyPad is a USB device running a CircuitPython program.

Plug the JoyPad into a computer and the sample program creates a USB mouse and USB keypad.

The JoyPad behavior and features are totally customizable using CircuitPython - an easy to use programming language. And if you really don't want to learn to program, that's OK too. The JoyPad is preprogrammed as a keypad and making small changes is a breeze without learning a bunch of code.

The API provides easy-to-use configuration and operations capabilities.

If you are just starting out with the JoyPad, take a look at the [**README**](README.md) for an overview and some easy customization examples to get you started!

[[_TOC_]] 


## JoyPad API

The JoyPad library provides a set of _methods_ and _properties_ which you use in your `code.py` to customize the behavior of you JoyPad device. 

>>>
Methods and properties are terms used in CircuitPython programming. They are mentioned here for completeness but are not significant for customizing the JoyPad. The terms _method_ and _property_ are common to many programming languages. A _method_ is a capability of a library to perform an action. A _property_ is a piece of information of the library which may get looked at and some _properties_ may be changed. _(not all properties may be changed_)
>>>

What follows are the details of the library and what each function or attribute does as well as what inputs/outputs each has.

The JoyPad library provides access to its general operations, the keys, joystick, screen, LEDs,  the buzzer, and has some convenient pre-defined keywords.

Here is a block diagram of the JoyPad library ...

<img align="center" style="width: 90%" src="files/block_diagram.png"/>

Before you can use the JoyPad library, you need to load it. Near the top of you `code.py` CircuitPython program, you will _import_ the library. The simplest method is with:

```python
from joypad import JoyPad   # REQUIRED: import the JoyPad library
```

### Operational Capabilities

#### JoyPad Constants

The JoyPad library begins with its operational capabilities. These include a number of pre-defined _properties_ to make programming easier and more readable. Each predefine property starts with `JoyPad.` and is then in all capital letters, as in `JoyPad.PRESSED` which is used to compare a key action to check if is pressed or released.

Here are all of the pre-defined properties:

| **Definition**    | **Description**                                      |
|:------------------|:--------------------------------------------------|
| `JoyPad.ON`       | used for operations expecting something as ON or OFF|
| `JoyPad.OFF`      | used for operations expecting something as ON or OFF|
| `JoyPad.LEFT`     | used for mouse operations to indicate the mouse button |
| `JoyPad.RIGHT`    | used for mouse operations to indicate the mouse button |
| `JoyPad.PRESS`    | used for mouse operations to indicate its action |
| `JoyPad.RELEASE`  | used for mouse operations to indicate its action |
| `JoyPad.PRESSED`  | used for key and button operations to indicate its status |
| `JoyPad.RELEASED` | used for key and button operations to indicate its status |
| `JoyPad.RED`      | used for changing LED color |
| `JoyPad.GREEN`    | used for changing LED color |
| `JoyPad.ORANGE`   | used for changing LED color |
| `JoyPad.YELLOW`   | used for changing LED color |
| `JoyPad.BLUE`     | used for changing LED color |
| `JoyPad.MAGENTA`  | used for changing LED color |
| `JoyPad.PURPLE`   | used for changing LED color |
| `JoyPad.PINK`     | used for changing LED color |
| `JoyPad.CYAN`     | used for changing LED color |
| `JoyPad.WHITE`    | used for changing LED color |
| `JoyPad.GRAY`     | used for changing LED color |
| `JoyPad.BLACK`    | used for changing LED color _(black will turn all of the LEDs off)_ |


#### JoyPad Instance

Your `code.py` CircuitPython program needs to create an _instance_ of the JoyPad library. This provides access to all of the capabilities of the library. The simplest method is with:

```python
jp = JoyPad() # this is the default for creating your JoyPad object
```

There are two optional _parameters_ which are available when creating the instance. You may provide a default `color` for all of the LEDs and you may set the JoyPad to `advanced` mode. Both of these choices may also be set after the instance is created.

For example, to create your JoyPad _instance_ in `advanced` mode and with the default LED color set to blue, the full method looks like this:

```python
jp = JoyPad(advanced=True, color=JoyPad.BLUE)
```

#### JoyPad Execution

Most of the JoyPad execution is handled by `update()`. This needs to be called frequently. Don't worry about calling it too often. The `update()` has an internal _speed limit_. The limit is controlled by the `rate` settings and under most circumstances should not be changed. _(see below)_

The update is performed within your `code.py` `while True:` loop and looks like this ...

```python
jp.update() # this is very important so everything has a chance to check for new information
```

The `update()` performs all the necessary periodic tasks including getting new information about the joystick and its push-button, updating the screen, and updating the LEDs.

#### JoyPad Settings

At anytime after you have created your JoyPad instance, you may set the default LED color, switch to/from advanced mode, and change the minimum update period. Here are examples of these three settings:

```python
jp.advanced = True
jp.color = JoyPad.WHITE
jp.rate = 20 # this is in milliseconds and caution is advised when changing this from its default
```

The **advanced** and **color** settings are the same as described above.

Changing the **rate** affects the _speed limit_ which the JoyPad will perform an update. The value is in milliseconds.

**CAUTION:** If you request and update more frequently than the value provided, the update will be skipped. If you request and update less often than the value provided, the update will always run. If the **rate** is a large number or update is performed often enough, it will affect the default performance of the joystick/mouse behavior. As a general rule, do not change the rate unless you know how quickly you will be able to perform the update.

### Keypad

The keypad is processed automatically and is independent of the `update()` operation.

The property `keys_changed` is `True` if any number of keys have been pressed or released. Each time `keys_changed` is checked, the data of the next key is loaded into the `key` property. 

The `key` has four pieces of information ...

```python
key.number      # the key number starts at 0
                # in normal mode, the first key is the left key of the second row
                # because the top row is reserved for mouse actions
                # in advanced mode, the first key is the left key of the top row
key.raw         # the raw number starts at 0 and is always the top left position
                # the raw value is convenient for controlling the LEDs
key.pressed     # True if the key was pressed, False if the key was released
key.timestamp   # a number measured in milliseconds
```

To process all key changes, you use a `while` loop ...

```python
while jp.keys_changed:
    # jp.key has the new key data
    number = jp.key.number
    action = jp.key.pressed
    ...
    # do something with the key
```

A common task for the JoyPad is to map key presses or releases to send keycodes or macros over USB to the computer. The `send()` makes this easy. 

```python
    jp.send(command)
```

The `command` parameter for `send()` may be a single keycode, a string, a combination of keycodes, or a combination of all of these.


| Type       | Description                                      |
|:-----------------|--------------------------------------------------|
| **string**        | send the contents of the string as if they were typed at a keyboard |
| **int**           | send a single [keycode](https://circuitpython.readthedocs.io/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode) |
| **set**           |  a group of keycodes within parenthesis () with all keys pressed together eg: `CTRL`+`C`, or `SHIFT`+`HOME` |
| **list**          | a list of keycodes within brackets [] with each keycode sent in succession |

Examples:
- a Keycode ([list of available keycodes](https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode))
  - `command = Keycode.ASTERISK`
  - `command = Keycode.LEFT_ARROW`
- a group of Keycodes (use parenthesis around the group and a comma to separate the Keycodes)
  - `command = (Keycode.CONTROL, Keycode.P)`
- a list of Keycodes (used brackets around the collection and a comma to separate the Keycodes)
  - `command = [Keycode.END, Keycode.PERIOD]`
- a strings of characters
  - `command = "hello world"`

The above options may be combined together:
 - `command = [Keycode.HOME, (Keycode.SHIFT, Keycode.END), Keycode.DELETE]`

### Joystick

The joystick generates X and Y data. It also as a single push-button action.

The X and Y data is processed and available as standard mouse movement. This data is available from JoyPad `movement`property. The raw X and Y data from the joystick has values in the range of -32K to +32K. This data is available from the JoyPad `position` property.

When the JoyPad is used in normal mode (vs advanced mode) the X and Y data is automatically processed and sent over USB as mouse movements.

Additionally, in normal mode, the top row of keys from the keypad are reserved for mouse related tasks. The left two keys are for left / right mouse buttons.

The second key from the right switches the USB mouse data from moving the mouse cursor to scrolling the active window on the computer.

The right most key changes the speed of mouse data - when the key is pressed, the mouse movement switches fast/slow depending on the `turbo` property.

The `turbo` property indicates the default speed of mouse movement relative to joystick movement. When `turbo = False`, the default mouse movement is slow. When `turbo = True`, the default mouse movement is fast. The right most key of the top row will temporarily swap the move movement speed while the key is pressed. _(see the 'Toggle Joystick Speed' example above)_

In advanced mode, the joystick data - `movement` and `position` - is available for any use. To manually control mouse movement, there is `mousemove(x, y, scroll)`. To manually perform mouse click actions, there is `mousebutton(button, action)`.

```python
    jp.mousemove(3, 3, 0) # move the mouse 3 pixes X and Y

    jp.mousebutton(JoyPad.RIGHT, JoyPad.PRESS)
    jp.mousebutton(JoyPad.RIGHT, JoyPad.RELEASE) # together this is right click
```

The joystick may be pressed down. This behaves similar to one of the key of the keypad with one added capability - toggle. The joystick `button_changed is True` if the button has been pressed or released since the last check of `button_changed`. Here is an example ...


```python
    if jp.button_changed:
        # jp.button has the new button data
        action = jp.button.pressed
    ...
    # do something with the key
```

The `button` has four pieces of information ...

```python
button.raw          # the raw value is convenient for controlling the LED
button.pressed      # True if the button was pressed
                    # False if the button was released
button.toggled      # True if the button was pressed and released
                    # False if the button was pressed and released again
button.timestamp    # a number measured in milliseconds
```


### Display

The display has a title line and up to five additional text lines. The title support up to 16 characters. The text lines support up to 21 characters.

The title is changed using `title(string)`. If the string is more than 16 characters long, it will be clipped. The title is centered at the top of the display.

```python
jp.title("JoyPad")
```

The five text lines are changed using either `textline(line_number, string)` or `printline(line_number, format, params)`. If the string is more than 21 characters long, it will be clipped. The text line is center in one of the 5 `line_number` positions. The `line_number` is a value from 0 to 4. The use of `printline()` is similar to CircuitPython `print()`.

**Note:** the font used for the text lines is a fixed width font. This allows more control of screen text. For example, a label and a value could be left / right justified on the bottom line of the display using ...

```python
    jp.printline(4,"%-18s%3d", ("Label:", num))
```

### LEDs

There are addressable RGB LEDs for each key and for the joystick. The LEDs are number from 0. The top row left key has LED 0. The bottom row third key has LED 18. The joystick button has LED 19.

The default LED color is set using the `color` property _(see `color` in 'JoyPad Settings' above)_

An LED color is changed using `led(num, color)`. Color may be one of the predefined colors _(see `JoyPad Constants` above)_ or a custom color. A custom color is created using parentheses around three numbers, representing red, green, and blue, e.g. `(162, 0, 37)` is crimson.

All of the LEDs may be changed at once using `leds(color)`.

The brightness of the LEDs is controlled by the `brightness` property.

```python
jp.brightness = 0.5         # brightness set to 50%
jp.leds(JoyPad.BLUE)        # set all LEDs to blue
jp.led(19, JoyPad.GREEN)    # set the joystick LED to green
```

### Buzzer

The JoyPad has a very simple buzzer. It is able to produce a simple tone. It will nto play sound files.

The buzzer may play a tone for a period of time or it may produce a tone until instructed to stop.

The tone is given as a frequency from 0 to 16K. In real terms, a value below 8K is recommended.

Use `tone(frequency, duration)` to play a tone for a fixed amount of time. The duration is in seconds and may be a decimal number, e.g. `0.5` is one half second.

**Caution:** The `tone()` will only finish after `duration`. Do not use a long duration if you are also moving the joystick or pressing keys because these tasks will wait until `tone()` completes.

An alternative to `tone()` is to use `toneon(frequency)` and `toneoff()`.

Here is an example of starting a tone, processing all of the keypad keys, and then stopping the tone ...

```python
    jp.toneon(440) # 'A' on the music scale

    while jp.keys_changed:
        key = jp.key.number
        action = jp.key.pressed
        jp.send(key_sequences[key])

    jp.toneoff()
```

### Direct Access

The JoyPad hardware may be used without the JoyPad library. Here are the CircuitPython board pins on the Raspberry Pi PICO and their assignments.

| Board Pin       | Usage                                      |
|:-----------------|--------------------------------------------------|
| **Board.A0**     | joystick horizontal axis |
| **Board.A1**     | joystick vertical axis |
| **Board.GP20**   | joystick button (open HIGH) |
| **Board.GP14**<br>**Board.GP13**<br>**Board.GP12**<br>**Board.GP11**<br>**Board.GP10**   | keypad rows |
| **Board.GP6**<br>**Board.GP7**<br>**Board.GP8**<br>**Board.GP9**   | keypad columns |
| **Board.GP21**<br>**Board.GP20**  | OLED display I2C (SCL and SDA) |
| **Board.GP16**   | Neopixels (total of 20) |
| **Board.GP18**   | Buzzer |
| **Board.GP17**   | Buzzer EN (HIGH for enabled, LOW for disabled) |



**Tip:** Check out [musicdemo.py](examples/musicdemo.py) for a complete example of using tones with the keypad. To load it onto the JoyPad, copy the file to `CIRCUITPY` with the destination name `code.py`

_(The [testing example](examples/testing.py) has examples are all of the properties and methods of the JoyPad library.)_
