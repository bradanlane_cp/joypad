# joypad.py (JoyPad)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library supports the JoyPad hardware.

Implementation Notes
--------------------
**Hardware:**
    * the JoyPad (available on Tindie) https://www.tindie.com/stores/bradanlane/

**Software and Dependencies:**
    * CircuitPython firmware (UF2): https://circuitpython.org/board/raspberry_pi_pico/
    * Adafruit CircuitPython Libraries: https://circuitpython.org/libraries
"""

import supervisor
import board
import time
import pwmio
import digitalio
import analogio
import busio
import displayio

import usb_hid
import keypad as keypad_module

try:
    import neopixel
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/neopixel.mpy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")
try:
    import adafruit_displayio_ssd1306
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_displayio_ssd1306.mpy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

try:
    #from adafruit_display_text import label
    from adafruit_display_text import bitmap_label
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_display_text'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

try:
    from adafruit_bitmap_font import bitmap_font
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_bitmap_font'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

try:
    #from adafruit_hid.keyboard import Keyboard
    #from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
    #from adafruit_hid.keycode import Keycode
    from adafruit_hid.mouse import Mouse
    from adafruit_hid.keyboard import Keyboard
    from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_hid'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

print ("")


class _JoyKey:
    """
    The JoyKey is a simplified version of keypad.event.
    The important difference is that JoyKey is read/write
    """
    def __init__(self):
        self.number = 0
        self.raw = 0
        self.pressed = False
        self.toggled = False
        self.timestamp = 0

    def __repr__(self):
        return "<number:%d raw:%d pressed:%s toggled:%s timestamp:%d>" % (self.number, self.raw, "True " if self.pressed else "False", "True " if self.toggled else "False", self.timestamp)

    def set(self, event, num=-1, toggled=False):
        if num < 0:
            self.number = event.key_number
            self.raw = event.key_number
        else:
            self.number = num
            self.raw = num
        self.pressed = event.pressed
        self.toggled = toggled
        self.timestamp = event.timestamp

    """
    def clear(self):
        print("clear")
        self.number = 0
        self.raw = 0
        self.pressed = False
        self.toggled = False
        self.timestamp = 0
    """

class _JoyBuzzer:
    """
        The JoyPad has a simple piezo buzzer available for tones.
        Tones are generated using code extracted from simpleio library.
    """
    STACK_SIZE = 8

    def __init__(self):
        _STACKSIZE = 6
        self._buzzer_power = digitalio.DigitalInOut(board.GP17)
        self._buzzer_power.direction = digitalio.Direction.OUTPUT
        self._buzzer_power.value = False
        self._pwm = None

    def toneon(self, frequency=880):
        if self._pwm is None:
            self._buzzer_power.value = True
            #simpleio.tone(board.GP18, frequency, duration=seconds)
            self._pwm = pwmio.PWMOut(board.GP18, frequency=int(frequency), variable_frequency=True)
            self._pwm.duty_cycle = 0x8000
        else:
            self._pwm.frequency = frequency

    def toneoff(self):
        if self._pwm is not None:
            self._pwm.deinit()
            self._pwm = None
            self._buzzer_power.value = False

    def tone(self, frequency=880, seconds=0.5):
        self.toneon(frequency)        
        time.sleep(seconds)
        self.toneoff()


class _JoyNeopixels:
    """ JoyNeopixels:
        The LEDs are addressable RGB and often called Neopixel LEDs.
        all of the LEDs are chained together and controlled together.

        There are 20 LEDs. The first 19 LEDs are under the keys of the keypad.
        The order of the LEDs is the same as the order of the keys
        with the exception of the last LED with is for the Joystick.

        NOTE: Python counts positions starting with 0, not 1.
            For example, neopixels[0] is the top left key,
            neopixels[16] is the bottom left, and 
            neopixels[19] is the Joystick.

        The default behavior is all of the keypad neopixels are white when the
        JoyPad is plugged into USB.

        The color and behavior of the keypad neopixels is customizable.
    """

    BLACK	= (  0,  0,  0)

    def __init__(self, color=BLACK):
        self._neopixels = neopixel.NeoPixel(board.GP16, 20, brightness=0.5, auto_write=False)
        self._neopixels.fill(color)	# start with all neopixels being on; yser can choose color or default to low intensity white
        self.show()

    def show(self):
        self._neopixels.show()

    def set(self, pixel=0, color=BLACK):
        self._neopixels[pixel] = color

    def setall(self, color=BLACK):
        self._neopixels.fill(color)

    @property
    def directaccess(self):
        return self._neopixels

    @property
    def size(self):
        return self._count

    @property
    def brightness(self):
        return self._neopixels.brightness
    @brightness.setter
    def brightness(self, brightness=0.3):
        brightness = brightness if brightness > 0.1 else 0.1
        brightness = brightness if brightness < 1.0 else 1.0
        self._neopixels.brightness = brightness

class _JoyScreen:
    """ 
        The JoyPad has an OLED display available for text output.
        It has one title line and a body area of five text lines. The body text lines are numbered 0..4.
        The title text has a maximum of 15 characters wide. The body text has a maximum of 22 characters

        When updated multiple lines of the display, it is faster to use refresh=False
        for the lines followed by a call to display().

        Tip: knowing the max width of a body line is 22 characters, use the printtext() method to left/right align content

        The available methods are:
            display()                                       # force a refresh of the display
            title(text='JoyPad', refresh=True)              # display text in the title area at the top of the screen
            textline(line=0, text='example', refresh=True)  # display text on one of the text lines
            printline(line=0, fmt='example %d %d', parms=(123, 456), refresh=True)  # print() syntax to display text on one of the text lines
    """
    """
        source of many fonts: https://github.com/olikraus/u8g2/tree/master/tools/font/bdf
        some fonts may cause an error. the most common two problems are:
            1) there 'copyright' text
            2) the bounding box is in the form of a string rather than four values
        BDF font format may be converted to PCF format using the following online tool:
            https://adafruit.github.io/web-bdftopcf/
    """

    def __init__(self):
        self._inited = False
        displayio.release_displays()
        i2c = None
        display_bus = None
        self._oled = None

        try:
            i2c = busio.I2C(board.GP21, board.GP20)
            display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)
            self._oled = adafruit_displayio_ssd1306.SSD1306(display_bus, width=128, height=64, auto_refresh=False)
        except:
            if i2c and (display_bus is None):
                print("Unable to locate OLED display at 0x3C")
                # we have I2C but not the display; we will dump a scan
                count = 0
                print("Scanning I2C bus")
                for x in i2c.scan():
                    print(hex(x))
                    count += 1
                print("%d device(s) found on I2C bus" % count)
                i2c.unlock()
                i2c = None

        if i2c is None:
            print("Unable to initialize I2C")
            return


        try:
            tfont = 'font/profont17.pcf'
            bfont = 'font/profont12.pcf'

            self._title_font = bitmap_font.load_font(tfont)
            self._body_font = bitmap_font.load_font(bfont)
        except:
            print("Warning: Missing '%s' or '%s'" % (tfont, bfont))
            print("The fonts are located in the project 'font' folder on gitlab")
            i2c.unlock()
            i2c = None
            pass
        if i2c is None:
            return

        #self._screens = [displayio.Group() for i in range (2)] # create two screens to allow background writes
        self._screen = displayio.Group()
        # create the title area to be the full width (' '*16 is slightly wider than the physical screen)
        #self._title_line = bitmap_label.Label(self._title_font, text=' '*16, background_color=0xFFFFFF, color=0x000000, background_tight=True)
        self._title_line = bitmap_label.Label(self._title_font, text=' '*16, color=0xFFFFFF)
        self._title_line.anchor_point = (0.5, 0.20)  # centered top
        self._title_line.anchored_position = (64, 0)  # middle top
        self._screen.append(self._title_line)

        self._body_lines = [bitmap_label.Label(self._body_font, text=' '*22, color=0xFFFFFF) for i in range (5)]
        for i in range(5):
            self._body_lines[i].anchor_point = (0.5, 0.0)  # centered top
            self._body_lines[i].anchored_position = (64, (i*10)+12)  # middle top
            self._screen.append(self._body_lines[i])

        self._inited = True
        self._oled.show(self._screen)
        #self.show()
        self.title('', False)
        self.show()

    def show(self):
        if self._inited:
            self._oled.refresh()
            pass

    def title(self, text='JoyPad', refresh=True):
        if self._inited:
            self._title_line.text = text
            if refresh:
                self.show()

    def textline(self, line=0, text=' ', refresh=True):
        if self._inited:
            self._body_lines[line].text = text
            if refresh:
                self.show()

    def printline(self, line=0, fmt='missing', parms=(0), refresh=True):
        if self._inited:
            buf = fmt % parms
            self.textline(line, buf, refresh)


class _JoyKeypad:
    """ JoyKeypad:
        The most common customization is to change the action of the keys.
        All actions are stored in the key_commands 'tuple' (aka static list).
        The order in the key_commands list is 'top to bottom' to match the
        the layout of the physical JoyPad.

        By default the top four keys are reserved for mouse left & right, scroll,  and turbo move.
        This leaves 15 customizable keys. With a standard keypad layout, there are 3 customizable keys.
        IMPORTANT: the last row only has three keys; the forth key must be left as 'None'

        NOTE: Python counts positions starting with 0, not 1.
    """

    # reserved keys with special meanings
    LEFT = 0
    RIGHT = 1
    SCROLL = 2
    TURBO = 3

    def __init__(self):
        self._keypad = keypad_module.KeyMatrix(
            row_pins=(board.GP14, board.GP13, board.GP12, board.GP11, board.GP10),
            column_pins=(board.GP6, board.GP7, board.GP8, board.GP9),
            columns_to_anodes=True
        )

    def has_event(self, event):
        return self._keypad.events.get_into(event)


class _JoyJoystick:
    """ Class for game controller type joysticks.
        These types of joysticks have analog input for
        horizontal and vertical movement plus a push
        button.

        This class has been tested with the XBOX JoyStick.

        Most controller joysticks have a very limited analog range vs 
        physical movement. The code implements a 'curve' mapping
        to make best use of the limited range. It may be necessary
        to adjust the curve() code to suit different joystick components
        or user preferences. There is also a "biased rolling average"
        of the X and Y data. The rolling average is biased to ramp to zero
        more quickly than it ramps from zero. This helps to stop the 
        output more quickly while still smoothing out the rate change
        if movements.

        Multiple curve maps are supported. The default code uses 2 maps, a normal map,
        and a very slow map. The very slow map is used for scrolling. A third map has 
        been created for fast cursor movement. A key may be used to switch between 
        normal and fast cursor motion.

        The default function of the push button is to toggle the joystick
        between mouse movement and scroll movement.

    """

    """ configuration """
    AVG_SAMPLE=5				# number of samples for each reading
    ROLLING_SIZE=10				# rolling average for smoothing = higher number is smoother but will add lag
    ANALOG_THRESHOLD=5000		# dead zone where mouse does not move; increase if the mouse wanders when not touched (I2C display is very noisy)

    """ movement curves (bit field) """
    NORMAL      = 0x00
    SCROLL      = 0x01
    TURBO       = 0x02
    # TURBOSCROLL = (SCROLL + TURBO)

    """
        a curve is a set of 4 pairs where each pair is a 'movement value' and a 'repeat counter'
        each one of the pairs corresponds to one of the four thresholds: 10000, 20000, 30000, and above
        anthing below the ANALOG_THRESHOLD will always result is a movement of zero

        you may edit these pairs or create new ones
    """

    def __init__(self):
        # , b_pin, h_pin, v_pin = (board.GP22, , )
        """
        Initialise the driver with the correct pins.
            :param board.PIN b_pin: digital pin for button press
            :param board.PIN h_pin: analog pin used for vertical movement
            :param board_PIN v_pin: analog pin used for horizontal movement
        """
        # private properties
        self._vertical_axis = analogio.AnalogIn(board.GP27_A1)
        self._horizontal_axis = analogio.AnalogIn(board.GP26_A0)
        self._button = keypad_module.Keys((board.GP22,), value_when_pressed=False, pull=True)
        self._button_event = keypad_module.Event()

        self._rolling_x = [0 for i in range (_JoyJoystick.ROLLING_SIZE)]
        self._rolling_y = [0 for i in range (_JoyJoystick.ROLLING_SIZE)]
        self._scroll_counter = 0;		# scroll counter
        self._vertical_counter = 0;    # scroll counter
        self._horizontal_counter = 0;	# scroll counter
        # motion curves: normal, scroll, turbo, turbo scroll
        self._curves_default = [((1,4),(1,2),(1,0),(2,0)),  ((1,32),(1,24),(1,16),(1,12)),  ((1,0),(2,0),(4,0),(8,0)),  ((1,32),(1,24),(1,12),(1,6))]
        self._curves = self._curves_default
        self.button_changed = False

        # public properties
        self.movement = [0,0,0]  # x, y, scroll
        self.position = [0,0,0]  # x, y, scroll (no curve)
        self.button_key = _JoyKey()
        self.recalibrate()


    def _raw_update(self):
        x = 0
        y = 0
        for i in range(_JoyJoystick.AVG_SAMPLE):
            x = x + self._horizontal_axis.value
            y = y + self._vertical_axis.value
        x = int(x / _JoyJoystick.AVG_SAMPLE)
        y = int(y / _JoyJoystick.AVG_SAMPLE)

        y = y - self._vertical_center	    # delta movement
        x = x - self._horizontal_center	# delta movement
        x = x if (abs(x) > _JoyJoystick.ANALOG_THRESHOLD) else 0
        y = y if (abs(y) > _JoyJoystick.ANALOG_THRESHOLD) else 0

        if not self._vertical_center == 0:	# don't flip until we have set the center
            x = -x	# flip X axis

        # update the button_pressed flag with the button current state
        self.button_changed = False # assume no new button state
        toggled = self.button_key.toggled
        if self._button.events.get_into(self._button_event):
            # if just released
            if self._button_event.released:
                toggled = not toggled
                """
                if not toggled:
                    self.recalibrate()
                """
            self.button_key.set(self._button_event, 19, toggled)    # 19 is the LED number
            self.button_changed = True
            #print("press-button: press:%d toggled:%d" % (self.pressed, self.toggled))

        return x, y

    def _rolling_average(self, x, y):
        """ rolling average smooths the analog values
        It is biased toward stopping more quickly than starting.
        This  means it will smooth values but when the values
        are zero, it will be quicker to settle.
        """
        x_inc = 1 if abs(x) > 0 else 2
        y_inc = 1 if abs(y) > 0 else 2
        for i in range (_JoyJoystick.ROLLING_SIZE - x_inc):
            self._rolling_x[i] = self._rolling_x[i+x_inc]
        if x == 0:
            self._rolling_x[_JoyJoystick.ROLLING_SIZE-2] = 0
        for i in range (_JoyJoystick.ROLLING_SIZE - y_inc):
            self._rolling_y[i] = self._rolling_y[i+y_inc]
        if y == 0:
            self._rolling_y[_JoyJoystick.ROLLING_SIZE-2] = 0

        self._rolling_x[_JoyJoystick.ROLLING_SIZE-1] = x
        self._rolling_y[_JoyJoystick.ROLLING_SIZE-1] = y

        x = 0
        y = 0
        for i in range (_JoyJoystick.ROLLING_SIZE):
            x = x + self._rolling_x[i]
            y = y + self._rolling_y[i]
        x = int(x / _JoyJoystick.ROLLING_SIZE)
        y = int(y / _JoyJoystick.ROLLING_SIZE)

        return x, y

    def _curve(self, x, y, mode=0):
        """ make a usable motion curve:
            the XBOX joysticks have a narrow range - the last 30-50% of travel is 100% value.
            There needs to be a dead zone in the middle to account for jitter.
            The algorithm divides the range into zones and will move from 1, 2, or 8
            and at lower values will output 1 only after a number of repeated skips.
        """

        mode = 0 if (mode > 3) or (mode < 0) else mode
        curve = self._curves[mode]
        values = [x, y]
        counts = [self._horizontal_counter, self._vertical_counter]
        if mode & _JoyJoystick.SCROLL:
            counts[0] = self._scroll_counter

        for i in range(2):
            count = counts[i]
            val = values[i]
            sign = -1 if val < 0 else 1

            if (abs(val) < _JoyJoystick.ANALOG_THRESHOLD):
                val = 0
                repeat = 0
                repeat_slow = 0
            elif (abs(val) < 7500):
                val = curve[0][0]
                repeat = curve[0][1]
            elif (abs(val) < 15000):
                val = curve[1][0]
                repeat = curve[1][1]
            elif (abs(val) < 25000):
                val = curve[2][0]
                repeat = curve[2][1]
            else:
                val = curve[3][0]
                repeat = curve[3][1]

            counts[i] = ((counts[i] + 1) % repeat) if repeat > 0 else 0
            values[i] = (val * sign) if counts[i] == 0 else 0

        if mode & _JoyJoystick.SCROLL:
            self._scroll_counter = counts[0]
        else:
            self._horizontal_counter = counts[0]
            self._vertical_counter = counts[1]

        return values[0], values[1]

    def _set_curve(self, position, curve):
        if curve is None:
            self._curve[position] = self._curves_default[position]
        else:
            self._curve[position] = curve
            
    def recalibrate(self):
        time.sleep(0.5)
        self._horizontal_center = 0
        self._vertical_center = 0
        x, y = self._raw_update()
        self._horizontal_center = x
        self._vertical_center = y
        #print("JoyStick Calibration: center = %5d, %5d " % (self._vertical_center, self._horizontal_center))

    def update(self, mode=0):
        """
        horizontal and vertical position of the joystick or scroll
        returns boolean if there is motion
        """
        
        x0, y0 = self._raw_update()
        # smooth out the input data
        x1, y1 = self._rolling_average(x0, y0)

        if mode & _JoyJoystick.SCROLL:
            scroll, y2 = self._curve(y1, 0, mode)
            x2 = 0
            y2 = 0
            scroll = -scroll    # reverse scroll direction
        else:
            x2, y2 = self._curve(x1, y1, mode)
            scroll = 0

        # save results
        self.position[0] = x1
        self.position[1] = y1
        self.position[2] = 0
        self.movement[0] = x2
        self.movement[1] = y2
        self.movement[2] = scroll

        """
        if x0 or y0:
            print("%05d,%05d => %05d,%05d => %05d,%05d" % (x0,y0,x1,y1,x2,y2))
        """
       
        if x2 or y2 or scroll:
            #print(f"X:{x2:-4d} Y:{y2:-4d} S:{scroll:-2d}")
            return True
        return False


class JoyPad:
    """ common colors """
    RED		= 0xff0000 #(255,  0,  0)
    GREEN	= 0x007f00 #(  0,127,  0)
    ORANGE  = 0xe04020 #(255, 93,  0)
    YELLOW	= 0xffff00 #(255,127,  0)
    BLUE	= 0x0000ff #(  0, 0, 255)
    MAGENTA	= 0xff00ff #(255,  0,255)
    PURPLE	= 0x3f007f #( 63,  0,127)
    PINK	= 0x7f007f #(127,  0,127)
    CYAN	= 0x00ffff #(  0,127,255)
    WHITE	= 0x7f7f7f #(127,127,127)
    GRAY	= 0x1f1f1f #( 31, 31, 31)
    GREY = GRAY
    DKGRAY	= 0x0f0f0f #( 15, 15, 15)
    BLACK	= 0x000000 #(  0,  0,  0)

    """ mosue buttons """
    LEFT = 0
    RIGHT = 1
    """ mouse actions """
    PRESSED = True
    RELEASED = False
    PRESS = True
    RELEASE = False
    """ toggle values """
    ON = True
    OFF = False


    def __init__(self, advanced=False, color=GRAY):

        # by default, the top row of keys are reserved for mouse actions
        # # the user may want to have control of all keys
        self.USB_DETECTED = True
        self._advanced = advanced
        self._default_color = color

        self._buzzer = _JoyBuzzer()
        self._neopixels = _JoyNeopixels(color)
        self._display = _JoyScreen()
        if not self._display._inited:
            for i in range(3):
                self._neopixels.setall(JoyPad.RED)
                self._neopixels.show()
                time.sleep(0.5)
                self._neopixels.setall(JoyPad.BLACK)
                self._neopixels.show()
                time.sleep(0.5)
            self._neopixels.setall(color)
            self._neopixels.show()
        self._keypad = _JoyKeypad()
        self._joystick = _JoyJoystick()

        """
        self._kbd = Keyboard(usb_hid.devices)
        self._kbd_layout = KeyboardLayoutUS(self._kbd)  # a US keyboard layout
        self._mouse = Mouse(usb_hid.devices)
        """
        try:
            self._kbd = Keyboard(usb_hid.devices)
            self._kbd_layout = KeyboardLayoutUS(self._kbd)  # a US keyboard layout
            self._mouse = Mouse(usb_hid.devices)
        except:
            #print ("Error: USB not detected. Operating in demo mode.")
            self.USB_DETECTED = False
            self._kbd = None
            self._kbd_layout = None
            self._mouse = None
            pass

        self._mouse_left = False
        self._mouse_right = False
        self._mouse_scroll = False
        self._mouse_turbo = False
        self._keypad_event = keypad_module.Event()
        self._dirty_leds = False
        self._dirty_display = False

        # public property 
        self.key = _JoyKey()
        self._turbo = True      # inverts turbo mouse movement
        self._update_rate = 10  # user can override how often we will attempt to update
        self._timer = SimpleTimer(self._update_rate)
        self._timer.start()


    def reset(self):
        supervisor.reload()


        
    """ Operations """
    def curve(self, position, curve):
        # this is experimental and not yet documented
        # the position is 0..3 = mouse, scroll, turbo mouse, turbo scroll
        # a curve a 4 tuple of 2 tuple eg ((1,16),(1,12),(1,8),(1,4))
        if (position < 0) or (position > 3):
            return
        self._joystick._set_curve(position, curve)

    def update(self):
        movement = False
        if self._timer.expired:
            self._timer.start()
            # modes are accumulative
            # allow inversion of effect of turbo key
            turbo = (not self._mouse_turbo) if self._turbo else (self._mouse_turbo)
            mode = _JoyJoystick.SCROLL if self._mouse_scroll else 0
            mode = mode + _JoyJoystick.TURBO if turbo else mode
            movement = self._joystick.update(mode)
            if not self._advanced:
                if movement:
                    self.mousemove()    # automatically process mouse
        if self._dirty_leds:
            self._neopixels.show()
            self._dirty_leds = False
        if self._dirty_display:
            self._display.show()
            self._dirty_display = False
        return movement

    @property
    def advanced(self):
        return self._advanced
    @advanced.setter
    def advanced(self, advanced=False):
        self._advanced = advanced

    @property
    def rate(self):
        return self._update_rate
    @rate.setter
    def rate(self, rate=10):
        if (rate < 1) or (rate > 10000):
            rate = 10
        self._update_rate = rate

    @property
    def color(self):
        return self._default_color
    @color.setter
    def color(self, color=None):
        if (color is not None) and (color != self._default_color):
            self.leds(color)
            self._default_color = color

    """ keypad and keys """
    @property
    def keys_changed(self): # returns a boolean True if there is new event data
        # in advanced mode, we return any key
        if self._advanced:
            found = self._keypad.has_event(self._keypad_event)
            if found:
                self.key.set(self._keypad_event)
            return found

        # in normal mode, we process the top row of keys and only return any of the other keys
        while True:
            if not self._keypad.has_event(self._keypad_event):
                return False

            num = self._keypad_event.key_number
            state = self._keypad_event.pressed
            if num > 3: # not one of the reserved keys
                self.key.set(self._keypad_event)
                self.key.number = self.key.number - 4   # 'number' takes into account if the top row is reserved 
                return True

            #self._neopixels.set(num, (JoyPad.RED if state else self._default_color))
            if num == 0:  # left mouse
                self._mouse_left = state
                self.mousebutton(JoyPad.LEFT, state)
            elif num == 1:  # right mouse
                self._mouse_right = state
                self.mousebutton(JoyPad.RIGHT, state)
            elif num == 2:  # scroll mode
                self._mouse_scroll = state
            elif num == 3:  # turbo mode
                self._mouse_turbo = state

    def send(self, command=None):
        if not self.USB_DETECTED:
            self.printline(4, "%s: %s", ("Send", repr(command)))
            return False
        if command == None:
            return False

        if isinstance(command, str):
            self._kbd_layout.write(command)     # output a text string
        elif isinstance(command, int):
            self._kbd.send(command)             # output a keycode
        elif isinstance(command, list):
            for cmd in command:                 # loop thru commands in list
                self.send(cmd)                  # recurse to output each command
        elif isinstance(command, tuple):
            for cmd in command:                    # output tuple by pressing all keys together
                self._kbd.press(cmd)           # press each key
            self._kbd.release_all()             # release all keys pressed
        else: # unknown
            # this is a debugging print message, only visible with a serial monitor
            print("Unknown key command: ", command)
            return False
        return True

    """ Joystick """
    def mousemove(self, x=0, y=0, s=0): # send mouse movement over USB. If no parameters are provided, it will use the most recent joystick data
        # x y scroll
        # if not advanced, and no values were provided, use the last recorded data
        if (not self._advanced) and (not (x or y or s)):
            x = self._joystick.movement[0]
            y = self._joystick.movement[1]
            s = self._joystick.movement[2]
        if not self.USB_DETECTED:
            self.printline(4, "Mouse: %d %d %d", (x, y, s))
        else:
            # ;prevent repeating the same movement
            self._joystick.movement[0] = 0
            self._joystick.movement[1] = 0
            self._joystick.movement[2] = 0
            if x or y or s:            
                self._mouse.move(x, y, s)

    @property
    def position(self): # return the most recent joystick x,y position values
        return self._joystick.position[0], self._joystick.position[1]

    @property
    def movement(self): # return the three most recent joystick movement values: three values X, Y, Scroll 
        return self._joystick.movement[0], self._joystick.movement[1], self._joystick.movement[2]

    def mousebutton(self, button, state): # send a mouse button (LEFT or RIGHT) action (PRESSED RELEASED) over USB
        if not self.USB_DETECTED:
            self.printline(4, "%s: %s", ("Right" if button else "Left", "Press" if state else "Release"))
            return False
        if state == JoyPad.PRESSED:
            if button == JoyPad.RIGHT:
                self._mouse.press(Mouse.RIGHT_BUTTON)
            else:
                self._mouse.press(Mouse.LEFT_BUTTON)
        else:
            if button == JoyPad.RIGHT:
                self._mouse.release(Mouse.RIGHT_BUTTON)
            else:
                self._mouse.release(Mouse.LEFT_BUTTON)

    @property
    def button(self):
        return self._joystick.button_key

    @property
    def button_changed(self):
        rtn = self._joystick.button_changed
        self._joystick.button_changed = False
        return rtn

    @property
    def turbo(self):
        return self._turbo
    @turbo.setter
    def turbo(self, turbo=True):
        self._turbo = turbo

    @property
    def scroll(self):
        return self._mouse_scroll
    @scroll.setter
    def scroll(self, scroll=True):
        self._mouse_scroll = scroll

    """ OLED Display """
    def title(self, title='JoyPad', refresh=False):
        if not self.USB_DETECTED:
            title = "Demo Mode"
        self._display.title(title, refresh)
        if not refresh:
            self._dirty_display = True

    def textline(self, line=0, text='', refresh=False):
        self._display.textline(line, text, refresh)
        if not refresh:
            self._dirty_display = True

    def printline(self, line=0, fmt='empty', parms=(0), refresh=False):
        self._display.printline(line, fmt, parms, refresh)
        if not refresh:
            self._dirty_display = True

    """ LEDs / Neopixels """
    def led(self, num, color, refresh=False):
        self._neopixels.set(num, color)
        if refresh:
            self._neopixels.show()
        else:
            self._dirty_leds = True

    def leds(self, color, refresh=False):
        self._neopixels.setall(color)
        if refresh:
            self._neopixels.show()
        else:
            self._dirty_leds = True

    @property
    def brightness(self):
        return self._neopixels.brightness
    @brightness.setter
    def brightness(self, brightness=0.3, refresh=False):
        self._neopixels.brightness = brightness
        if refresh:
            self._neopixels.show()
        else:
            self._dirty_leds = True
    @property
    def neopixels(self):
        return self._neopixels.directaccess

    """ Buzzer """
    def tone(self, freq=440, duration=0.05):
        self._buzzer.tone(freq, duration)

    def toneon(self, freq=440):
        self._buzzer.toneon(freq)

    def toneoff(self):
        self._buzzer.toneoff()






"""
supervisor.ticks_ms() is a counter which wraps
to accomodate for the wrapping, we provide the following class

Code derived from:
https://docs.circuitpython.org/en/latest/shared-bindings/supervisor/
"""
class SimpleTimer:
    _TICKS_PERIOD = const(1<<29)
    _TICKS_MAX = const(_TICKS_PERIOD-1)
    _TICKS_HALFPERIOD = const(_TICKS_PERIOD//2)

    def __init__(self, interval=100):
        self._interval = interval
        self._milliseconds = 0
        self._running = False

    @property
    def interval(self):
        return self._interval
    @interval.setter
    def interval(self, interval):
        self._interval = interval

    def start(self, interval=None):
        if interval is None:
            interval = self._interval
        self._milliseconds = supervisor.ticks_ms() + self._interval
        self._running = True
    def stop(self):
        self._milliseconds = 0
        self._running = False
    @property
    def expired(self):
        if self._running == False:
            return False
        # "Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"
        now = supervisor.ticks_ms()
        diff = (self._milliseconds - now) & _TICKS_MAX
        diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
        if diff < 0:
            self.stop()
            return True
        return False
        
