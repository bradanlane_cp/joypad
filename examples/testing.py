# code.py (JoyPad)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates nearly all of the JoyPad capabilities.

Documentation: 
**Hardware:**
    * the JoyPad (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

try:
    from joypad import JoyPad
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/joypad.py'")
    print ("The latest JoyPad library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/joypad")
    print("------------------------------------------------------------------------")

# IMPORTANT: the JoyPad uses a US keyboard layout. See documentation.
from adafruit_hid.keycode import Keycode

# a list of Keycodes is available here: 
# https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode
# use None for the buttons you wish to ignore

# this is an example of a compound key sequence; it will clear out a field in a form
clearL = [Keycode.HOME, (Keycode.SHIFT, Keycode.END), Keycode.DELETE]

# each key sequence may be a keycode, series of keycodes, keycode combinations, astring, or None
key_sequences = (\
    Keycode.SEVEN,  Keycode.EIGHT,  Keycode.NINE,   Keycode.ESCAPE, \
    Keycode.FOUR,   Keycode.FIVE,   Keycode.SIX,    clearL, \
    Keycode.ONE,    Keycode.TWO,    Keycode.THREE,  Keycode.ENTER, \
    None,  Keycode.ZERO,   Keycode.PERIOD, \
)

jp = JoyPad(color=JoyPad.WHITE)
jp.turbo = True
jp.advanced = False

jp.toneon(880)
jp.title('TEST')
#              "123456789012345678901" this is the maximum length of a text line
jp.textline(0, "LEFT RGHT SCRLL SLOW")
jp.textline(1, " 7     8    9    ESC")
jp.textline(2, " 4     5    6   clrL")
jp.textline(3, " 1     2    3    ENT")
jp.textline(4, "adv    0    .       ")
jp.toneoff()

jp.tone(440, 0.2)    # short beep to signals we are ready


colors = [JoyPad.RED, JoyPad.GREEN, JoyPad.BLUE, JoyPad.WHITE]
color = 0
count = 0

while True:
    jp.update() # this is very important so everything has a chance to check for new information

    # check the joystick push button
    if jp.button_changed:
        if jp.button.pressed:
            count = (count + 1) % 10
            jp.brightness = (count / 10)
            if count == 0:
                color = (color + 1) % 4
                jp.color = colors[color]
        # detect the joystick push-button has toggled
        if jp.button.toggled:
            jp.title("TEST TOGGLED")
        else:
            jp.title("TEST NORMAL")


    # in normal mode, the mouse buttons and movement is handled automatically
    # for demonstration, purposes, we do something a bit crazy by swapping mouse X/Y and manually calling mousemove()

    x, y, s = jp.movement
    if (x or y or s):
        jp.printline(0, "x:%02d y:%02d s:%02d", (x, y, s))

    # the code below uses key # 16 (the bottom left) to toggle in/out of advanced mode

    # process any key press/release changes
    while jp.keys_changed:
        key = jp.key.number
        action = jp.key.pressed

        if (jp.key.raw == 16) and (not jp.key.pressed): # released key #16
            jp.advanced = (not jp.advanced)
            if jp.advanced:
                jp.printline(0, "%-11s%11s", ("left", "right"))
            else:
                jp.textline(0, "LEFT RGHT SCRL TRBO")
                jp.leds(jp.color)

        if (jp.key.raw == 7) and (not jp.key.pressed): # released key #7
            jp.turbo = (not jp.turbo)
            if not jp.turbo: # the TURBO key swaps whatever 'turbo' is  (and the label)
                jp.textline(0, "LEFT RGHT SCRL SLOW")
            else:
                jp.textline(0, "LEFT RGHT SCRL FAST")


        # we could turn the LED a different color while the key is pressed
        if not jp.advanced:
            jp.led(jp.key.raw, JoyPad.ORANGE if action == JoyPad.PRESSED else jp.color)
            # ignore the key 'released' action
            if not action:
                continue
            # send key commands over USB
            jp.send(key_sequences[key])
        else:
            # in advanced mode we make the color 'sticky'
            jp.led(jp.key.raw, JoyPad.YELLOW)
            # we manually handle the mouse button actions
            if key == 0:  # left mouse
                jp.mousebutton(JoyPad.LEFT, action)
            elif key == 1:  # right mouse
                jp.mousebutton(JoyPad.RIGHT, action)
    # end while loop of new keypad events 

# end while True (forever)
